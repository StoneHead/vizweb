<?php

namespace Tomi\System;


class Bootstrap
{
    const ACTION_404 = '404';


    private $defaultComponents = array("Config", "Db");
    /**
     * @var \Tomi\System\Components\Config;
     */
    protected $config;

    /**
     * @var \Tomi\System\Components\Db
     */
    protected $db;

    protected $model;

    protected $modelPath;

    protected $viewPath;

    protected $systemPath;


    /**
     * Bootstrap constructor.
     * Project main things init.
     */
    public function __construct($action)
    {
        if ($action != self::ACTION_404) {
            $this->loadComponents();
            $this->loadConfig();
            $this->dbInit();

            $this->modelPath = getcwd() . "/Model/";
            $this->viewPath = getcwd() . "/View/";
            $this->systemPath = getcwd() . "/System/";

            $this->loadModel(explode('\\', get_class($this))[2]);
            session_start();
            $this->callAction($action);
        } else {
            $this->show_404();
        }

    }

    protected function callAction($action)
    {
        if (method_exists($this, $action)) {
            $this->$action();
        } else {
            $this->show_404();
        }
    }

    private function loadComponents()
    {
        foreach ($this->defaultComponents as $defaultComponent) {
            require("Components/" . $defaultComponent . ".php");
        }
    }

    /**
     * Db connect.
     */
    protected function dbInit()
    {
        $this->db = new namespace\Components\Db($this->config->get('db'));
    }


    /**
     * Config Loading
     */
    protected function loadConfig()
    {
        $this->config = new namespace\Components\Config();

    }

    protected function loadModel($model)
    {
        $model = ucfirst(mb_strtolower($model));
        $modelFile = $this->modelPath . $model . ".php";
        if (is_file($modelFile)) {
            require_once($modelFile);
            $className = "\\Tomi\\Model\\$model";
            $this->model = new $className($this->config->get('db'));
        } else {
            var_dump(($modelFile));
            die();
            $this->model = null;
        }
    }


    public function show_404()
    {
        echo "Sorry the requested page was not found!";
    }

    public function loadView($view, $data = array())
    {
        extract($data);

        // Start output buffering
        ob_start();
        //headbetöltés
        require($this->viewPath . "Layout/head.phtml");
        //current view betöltés
        $view = $this->viewPath . $view . ".phtml";
        require($view);

        //Footer betöltést
        require($this->viewPath . "Layout/footer.phtml");

        // End buffering and return its contents
        $output = ob_get_clean();

        print $output;
    }

    public function formatAlert($msg, $alertType = 'danger')
    {
        return "<div class=\"alert alert-$alertType\"><strong>$alertType!</strong> $msg</div>";
    }

    public function storeMsg($msg)
    {
        if (!isset($_SESSION['msg'])) {
            $_SESSION['msg'] = '';
        }
        $_SESSION['msg'] .= $msg;
    }

    public function getStoredMsg()
    {
        if (!isset($_SESSION['msg']) || $_SESSION['msg'] == '') {
            return null;
        }
        $msg = $_SESSION['msg'];
        $_SESSION['msg'] = '';
        return $msg;
    }

    public function reloadToAction($action)
    {
        header("Location: ?action=$action");
    }
}