<?php

namespace Tomi\System\Components;


class Db
{
    /**
     * @var \PDO
     */
    public $pdo;

    public function __construct($dbConfig)
    {
        try {
            $conn = new \PDO("mysql:host={$dbConfig['serverName']};dbname={$dbConfig['dbName']}", $dbConfig['userName'], $dbConfig['password']);
            // set the PDO error mode to exception
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo = $conn;
        } catch (\PDOException $e) {
            $this->pdo = null;
        }
    }


}