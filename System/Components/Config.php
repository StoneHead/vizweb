<?php


namespace Tomi\System\Components;


class Config
{
    /**
     * Config path
     * @var string
     */
    private $configPath = "./Config/";

    /**
     * @var array;
     */
    public $config;

    public function __construct()
    {
        foreach (scandir($this->configPath) as $key => $item) {
            if (!in_array($item, array('.', '..'))) {
                $this->config[substr($item, 0, strpos($item, "."))] = require($this->configPath . $item);
            }
        }
    }

    /**
     * @param $item
     * @return mixed|null
     */
    public function get($item)
    {
        if (isset($this->config[$item])) {
            return $this->config[$item];
        }
        return null;
    }
}