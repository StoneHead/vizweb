<?php

namespace Tomi\System;


class MainModel
{

    /**
     * @var \Tomi\System\Components\Db;
     */
    protected $db;

    public function __construct($config)
    {

        $this->dbInit($config);

    }

    public function insertInto($tableName, $data)
    {
        $colNames = "";
        $valNames = "";
        foreach ($data as $key => $item) {
            if (!is_null($item)) {
                $colNames .= $key . ",";
                $valNames .= "\"$item\",";
            }
        }

        $colNames = substr($colNames, 0, -1);
        $valNames = substr($valNames, 0, -1);

        $sql = "INSERT INTO $tableName ($colNames)VALUES ($valNames)";
        $statement = $this->db->pdo->prepare($sql);
        try {

            $statement->execute();
            return true;
        } catch (\PDOException $exception) {
            echo "PDO error :" . $exception->getMessage();
            return false;
        }

    }

    public function getWhere($table, $where = null, $select = '*')
    {
        $sql = "Select $select from $table {$this->formatWhere($where)};";

        $statement = $this->db->pdo->prepare($sql);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function deleteWhere($table, $where)
    {
        if (is_array($where) && !empty($where)) {
            $sql = "Delete from $table {$this->formatWhere($where)};";
            $statement = $this->db->pdo->prepare($sql);
            $statement->execute();

            if ($statement->rowCount() > 0) {
                return true;
            }
        }

        return false;
    }

    public function formatWhere($where)
    {
        $sql = '';
        if (!is_null($where) && !empty($where)) {
            $sql .= "WHERE ";
            foreach ($where as $key => $item) {
                $sql .= "`$key` = '$item'";
            }
        }

        return $sql;
    }


    /**
     * @param $config
     */
    protected function dbInit($config)
    {
        $this->db = new namespace\Components\Db($config);
    }
}