<?php

namespace Tomi\Controllers;

use Tomi\System\Bootstrap;

class Account extends Bootstrap
{

    private $signUpFields = array(
        'name' => array(
            'required' => 1,
            'type' => 'text',
            'unique' => 0,
        ),
        'email' => array(
            'required' => 1,
            'type' => 'text',
            'unique' => 1,
        ),
        'age' => array(
            'required' => 1,
            'type' => 'number',
            'unique' => 0,
        ),
        'gender' => array(
            'required' => 0,
            'options' => array('girl', 'guy'),
            'type' => 'text',
            'unique' => 0,
        ),
        'semester' => array(
            'required' => 0,
            'options' => array(1, 2, 3, 4, 5, 6, 7),
            'type' => 'number',
            'unique' => 0,
        ),
        'description' => array(
            'required' => 0,
            'type' => 'text',
            'unique' => 0,
        ),
    );

    /**
     * @var \Tomi\Model\Account;
     */
    protected $model;

    private $validateMsg;

    protected $tableName = 'users';

    public function __construct($action)
    {
        parent::__construct($action);
    }

    public function signup()
    {
        if ($this->validate()) {
            //insert db
            $data = array();
            foreach ($this->signUpFields as $key => $signUpField) {
                if (isset($_POST[$key])) {
                    $data[$key] = $_POST[$key];
                } else {
                    $data[$key] = null;
                }
            }

            //dbInsert
            if ($this->model->insertInto($this->tableName, $data)) {
                $this->reloadToAction('successSignUp');
            } else {
                //db be ilelsztési hiba!
            }
        } else {
            $this->loadView('Account/signup');
        }
    }

    public function successSignUp()
    {
        $this->loadView('Account/successSingUp');
    }

    public function delete()
    {
        $id = null;
        if (isset($_GET['password']) && $_GET['password'] == $this->config->get('main')['userDeletePassword']) {
            if (isset($_GET['userID'])) {
                $id = $_GET['userID'];
            }
            if (!is_null($id)) {
                if ($this->model->deleteWhere($this->tableName, array('id' => $id))) {
                    $this->loadView('Account/delete');
                    $this->storeMsg($this->formatAlert('Successful user delete!', 'success'));
                    $this->reloadToAction('users');
                } else {
                    $this->storeMsg($this->formatAlert('User delete failed!'));
                    $this->reloadToAction('users');
                }
            }
        } else {
            $this->storeMsg($this->formatAlert('Incorrect password!'));
            $this->reloadToAction('users');
        }

    }

    public function users()
    {
        $users = $this->model->getWhere($this->tableName);
        $data = array('users' => $users);
        $data['translateValues'] = array(
            'gender' => array('No' => 'Nő', 'Ferfi' => 'Férfi'),
        );
        $msg = $this->getStoredMsg();
        if (!is_null($msg)) {
            $data['msg'] = $msg;
        }

        $this->loadView('Account/users', $data);
    }

    public function home()
    {
        $this->loadView('Account/home');
    }

    public function userMail()
    {

        // the message

        if (isset($_POST['message'])) {
            $errorSendTo = '';

            // send email

            $headers = "Reply-To: The Sender <no-reply@mokx1.xyz>\r\n";
            $headers .= "Return-Path: The Sender <no-reply@mokx1.xyz>\r\n";
            $headers .= "From: The Sender <no-reply@mokx1.xyz>\r\n";
            $headers .= "Organization: Szakest\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/plain; charset=iso-8859-2\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";

            foreach ($this->model->getWhere($this->tableName, null, 'email') as $item) {
                if (!mail($item['email'], "Szakest", $_POST['message'], $headers)) {
                    $errorSendTo .= $this->formatAlert("Could not send mail to " . $item['email']);
                }
            }
            if ($errorSendTo != '') {
                $this->storeMsg($errorSendTo);
            } else {
                $this->storeMsg($this->formatAlert('User mail has been sent!', 'success'));
            }

            $this->reloadToAction('userMail');
        } else {
            $data = array();
            $msg = $this->getStoredMsg();
            if (!is_null($msg)) {
                $data['msg'] = $msg;
            }
            $this->loadView('Account/userMail', $data);
        }

    }

    /**
     * @return bool
     */
    private function validate()
    {
        $this->validateMsg = "";
        $_SESSION['errorMsg'] = "";
        $success = true;
        if (empty($_POST)) return false;
        foreach ($this->signUpFields as $key => $signUpField) {

            if ((!isset($_POST[$key]) || $_POST[$key] == "") && $signUpField['required'] == 1) {
                $this->validateMsg .= $this->formatAlert("The {$key} field is required!");
                $success = false;
            }
            if (!isset($_POST[$key])) {
                continue;
            }

            if ($signUpField['unique'] == 1 && count($this->model->getWhere($this->tableName, array($key => $_POST[$key]))) > 0) {
                $this->validateMsg .= $this->formatAlert("The {$key} field is unique! The \"$_POST[$key]\" has been already used!");
                $success = false;
            }

            if ($signUpField['type'] == 'text' && !is_string($_POST[$key])) {
                $this->validateMsg .= $this->formatAlert("The {$key} field has to be string!");
                $success = false;
            }
            if ($signUpField['type'] == 'number' && !is_numeric($_POST[$key])) {
                $this->validateMsg .= $this->formatAlert("The {$key} field has to be number!");
                $success = false;
            }

            if (isset($signUpField['option']) && !in_array($_POST[$key], $signUpField['option'])) {
                $this->validateMsg .= $this->formatAlert("The {$key} field has invalid value!");
                $success = false;
            }

        }

        $_SESSION['errorMsg'] = $this->validateMsg;
        return $success;
    }

}