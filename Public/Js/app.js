function getUserDeletePassword(event) {
    event.preventDefault();
    var password = prompt("Enter user delete password!");

    if (password != null) {
        window.location.href = event.target.getAttribute('href') + "&password=" + password;
    }
}