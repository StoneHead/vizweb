<?php

namespace Tomi;


class Router
{


    private $availableActions = array('home', 'users', 'signup', 'delete', 'userMail', 'successSignUp');
    private $availableControllers = array('account');
    private $fileName = null;
    private $className = null;
    private $action = null;

    public function __construct()
    {
        require_once("System/Bootstrap.php");
        require_once("System/MainModel.php");

        $get = &$_GET;
        $action = null;
        $controller = null;

        $controller = ucfirst(strtolower($this->availableControllers[0]));
        $this->fileName = "Controllers/{$controller}.php";
        $this->className = "\\Tomi\\Controllers\\{$controller}";


        $this->getClassAndFileName($get);

        $this->getAction($get);

        $this->load();

    }

    private function getClassAndFileName($get)
    {
        //Controller detection
        $className = $this->className;
        $fileName = $this->fileName;
        if (isset($get['controller']) && in_array($get['controller'], $this->availableControllers)) {
            $controller = $get['controller'];
            $controller = ucfirst(strtolower($controller));
            $fileName = "Controllers/{$controller}.php";
            $className = "\\Tomi\\Controllers\\{$controller}";
        }
        $this->fileName = $fileName;
        $this->className = $className;
    }

    private function getAction($get)
    {
        //Action detection
        if (isset($get['action'])) {
            if (in_array($get['action'], $this->availableActions)) {
                $this->action = $get['action'];
            } else {
                $this->action = '404';
            }
        } else {
            $this->action = $this->availableActions[0];
        }
    }

    public function load()
    {
        require_once($this->fileName);
        new $this->className($this->action);
    }


}